package com.hedspi.Lab02.ex02;

import java.util.Scanner;

public class InputFromKeyboard {
    public static void main(String[] args) {
        //Java co 3 doi tuong cho cac luong vao-ra co ban
        //System.out --> in ra console
        //System.in --> nhan du lieu tu ban phim
        //System.error --> thong bao loi tren console
        //1. Khoi tao doi tuong Scanner
        Scanner sc = new Scanner(System.in);

        // 2. Doc du lieu nhap vao: goi cac phuong thuc nextX() tu doi tuong Scanner,
        // X: thay bang ten doi tuong tuong ung
        System.out.println("Nhap ten cua ban: ");
        String name = sc.nextLine();

        System.out.println("Nhap tuoi cua ban: ");
        int age = sc.nextInt();

        System.out.println("Nhap chieu cao (met): ");
        Double heights = sc.nextDouble();

        System.out.println("Xin chao " + name +", ban " + age + " tuoi, cao " + heights + " met");
    }
}