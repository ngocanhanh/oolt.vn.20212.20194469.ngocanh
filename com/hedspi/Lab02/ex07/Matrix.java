package com.hedspi.Lab02.ex07;

import java.util.Scanner;

public class Matrix {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.print("Input num of col: ");
        int col = in.nextInt();
        System.out.print("Input num of row: ");
        int row = in.nextInt();
        int[][] a = new int[row][col];
        int[][] b = new int[row][col];
        for (int r = 1; r <= row; r++) {
            for (int c = 1; c <= col; c++) {
                System.out.print("a[" + r + "][" + c + "] = ");
                a[r-1][c-1] = in.nextInt();
            }
        }
        for (int r = 1; r <= row; r++) {
            for (int c = 1; c <= col; c++) {
                System.out.print("b[" + r + "][" + c + "] = ");
                b[r-1][c-1] = in.nextInt();
            }
        }
        System.out.println("Result :");
        for (int r = 1; r <= row; r++) {
            for (int c = 1; c <= col; c++) {
                System.out.print(a[r-1][c-1] + b[r-1][c-1] + " ");
            }
            System.out.println();
        }
    }
}
