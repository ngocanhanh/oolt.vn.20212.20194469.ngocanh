package com.hedspi.Lab02.ex05;

import java.util.Scanner;

public class NumOfDaysOfAMonth {
    public static void main(String[] args) {
        int year = 0, month = 0;
        int numOfDays = 0;
        Scanner sc = new Scanner(System.in);
        try {
            System.out.println("Nhap nam ");
            year = sc.nextInt();
            System.out.println("Nhap thang ");
            month = sc.nextInt();
        } catch (Exception e) {
            System.out.println("Loi nhap sai dinh dang!");
        }
        System.out.print(month + "/" + year);
        switch (month){
            case 2:
                if (year % 4 == 0 && (year % 100 != 0 || year % 400 == 0)) {
                    numOfDays = 29;
                } else numOfDays = 28;
                break;
            case 1:
            case 3:
            case 5:
            case 7:
            case 8:
            case 10:
            case 12:
                numOfDays = 31;
                break;
            case 4:
            case 6:
            case 9:
            case 11:
                numOfDays = 30;
                break;
            default:
                System.out.println("Loi nhap sai dinh dang!");
        }
        System.out.println(" co " + numOfDays + " ngay");
    }
}
