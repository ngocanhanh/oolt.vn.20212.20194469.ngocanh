package com.hedspi.Lab10.aims.exceptions;

public class AddMediaException extends Exception{
    private static final long serialVersionUID = 1L;

    public AddMediaException(String msg) {
        super(msg);
    }
}

