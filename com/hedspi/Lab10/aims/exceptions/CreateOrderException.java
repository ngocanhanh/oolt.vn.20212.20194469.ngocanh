package com.hedspi.Lab10.aims.exceptions;


public class CreateOrderException extends Exception{
    private static final long serialVersionUID = 1L;

    public CreateOrderException(String msg) {
        super(msg);
    }
}
