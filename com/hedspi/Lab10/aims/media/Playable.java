package com.hedspi.Lab10.aims.media;

import com.hedspi.Lab10.aims.exceptions.PlayerException;

public interface Playable {
    public void play() throws PlayerException;
}