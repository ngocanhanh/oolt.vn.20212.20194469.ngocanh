package com.hedspi.java;

/*
Solving Quadratic Equation
 */

import javax.swing.*;

import static java.lang.Math.sqrt;

public class BT6 {
    public static void main(String[] args) {
        double a, b, c, delta, x1, x2;
        a = Double.parseDouble(JOptionPane.showInputDialog(null, "a = ", "ax^2 + bx + c = 0", JOptionPane.INFORMATION_MESSAGE));
        b = Double.parseDouble(JOptionPane.showInputDialog(null, "b = ", "ax^2 + bx + c = 0", JOptionPane.INFORMATION_MESSAGE));
        c = Double.parseDouble(JOptionPane.showInputDialog(null, "c = ", "ax^2 + bx + c = 0", JOptionPane.INFORMATION_MESSAGE));
        if (a == 0) {
            x1 = -c/b;
            JOptionPane.showMessageDialog(null, "x = " + x1);
        } else {
            delta = b*b - 4*a*c;
            if (delta == 0) {
                x1 = -b/(2*a);
                JOptionPane.showMessageDialog(null, "x = " + x1);
            } else if (delta > 0) {
                x1 = (-b + sqrt(delta)) / (2*a);
                x2 = (-b - sqrt(delta)) / (2*a);
                JOptionPane.showMessageDialog(null, "x1 = " + x1 + "\n" + "x2 = " + x2);
            } else {
                JOptionPane.showMessageDialog(null, "No solution!");
            }
        }
        System.exit(0);
    }
}
