package com.hedspi.java;
/*
addition, subtraction, multiplication, division
 */
import javax.swing.JOptionPane;

public class BT5 {
    public static void main(String[] args) {
        double num1, num2, sum, diff, product, quotient;

        num1 = Double.parseDouble(JOptionPane.showInputDialog(null, "Please enter your first number: ", "Enter the first number", JOptionPane.INFORMATION_MESSAGE)
        );
        num2 = Double.parseDouble(JOptionPane.showInputDialog(null, "Please enter your second number: ", "Enter the second number", JOptionPane.INFORMATION_MESSAGE)
        );
        sum = num1 + num2;
        diff = num1 - num2;
        product = num1 * num2;
        quotient = num1 / num2;
        JOptionPane.showMessageDialog(null, num1 + " + " + num2 + " = " + sum + "\n" + num1 + " - " + num2 + " = " + diff + "\n" + num1 + " x " + num2 + " = " + product + "\n" + num1 + " / " + num2 + " = " + quotient);
        System.exit(0);
    }
}
