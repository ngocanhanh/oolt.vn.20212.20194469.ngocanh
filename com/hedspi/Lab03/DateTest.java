package com.hedspi.Lab03;

public class DateTest {
    public static void main(String[] args) {
        MyDate date1 = new MyDate();
        MyDate date2 = new MyDate("25th", "May", 2001);
        MyDate date3 = new MyDate("February 18th 2019");

         date1.printDate();
         date2.printDate();
         date3.printDate();

         date1.accept();
         date1.printDate();

         date3.print();
    }
}
