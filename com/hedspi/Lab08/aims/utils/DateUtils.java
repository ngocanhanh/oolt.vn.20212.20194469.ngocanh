package com.hedspi.Lab08.aims.utils;

//import com.hedspi.Lab07.aims.utils.MyDate;

public class DateUtils {
    // return 0 => date1 is after date2
    // return 1 => date1 is before date2
    // return -1 => date1 equals date2
    public static int compareTwoDates(MyDate d1, MyDate d2) {
        int month1 = Integer.parseInt(d1.getMonth());
        int month2 = Integer.parseInt(d2.getMonth());
        int day1 = Integer.parseInt(d1.getDay());
        int day2 = Integer.parseInt(d2.getDay());
        if (d1.getYear() > d2.getYear()) {
            return 0;
        }
        else if (d1.getYear() < d2.getYear()) {
            return 1;
        }
        else {
            if (month1 > month2) {
                return 0;
            }
            else if(month1 < month2) {
                return 1;
            }
            else {
                if (day1 > day2) {
                    return 0;
                }
                else if (day1 < day2) {
                    return 1;
                }
                else {
                    return -1;
                }
            }
        }
    }

    public static void printResultCompare(int result, MyDate date1, MyDate date2) {
        if (result == 1) {
            date2.print();
            System.out.print(" is the previous date of ");
            date1.print();
            System.out.println(" ");
        } else if(result == -1) {
            date1.print();
            System.out.print(" is the previous date of ");
            date2.print();
            System.out.println(" ");
        } else if (result == 0){
            date1.print();
            date2.print();
            System.out.println("are a same date!");
        }
    }
    public static void swapDate(MyDate d1, MyDate d2) {
        int tmpYear = d1.getYear();
        String tmpMonth= d1.getMonth();
        String tmpDay = d1.getDay();
        d1.setYear(d2.getYear());
        d1.setMonth(d2.getMonth());
        d1.setDay(d2.getDay());
        d2.setYear(tmpYear);
        d2.setMonth(tmpMonth);
        d2.setDay(tmpDay);
    }
    public static void sortDates(MyDate [] arrDates) {
        for (int i = 0; i < arrDates.length; i++) {
            for( int j = i + 1; j < arrDates.length; j ++) {
                if (DateUtils.compareTwoDates(arrDates[i],arrDates[j]) == 1) {
                    swapDate(arrDates[i],arrDates[j]);
                }
            }
        }
        System.out.println("Sorting a number of dates:");
        for (int i = 0; i < arrDates.length; i++) {
            arrDates[i].print();
            System.out.println(" ");
        }
    }
}
