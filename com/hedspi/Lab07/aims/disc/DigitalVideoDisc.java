package com.hedspi.Lab07.aims.disc;

import com.hedspi.Lab07.aims.media.Media;

public class DigitalVideoDisc extends Media {
    // Khai báo thuộc tính
    protected String director;
    protected int length;

    // Xây dựng phuơng thức
    //Các phương thức getter và setter
    public String getTitle(){
        return this.title;
    }

    public void setTiltle(String title){
        this.title = title;
    }

    public String getCategory(){
        return this.category;
    }

    public void setCategory(String category){
        this.category = category;
    }

    public String getDirector(){
        return this.director;
    }

    public void setDirector(String director){
        this.director = director;
    }

    public int getLength(){
        return this.length;
    }

    public void setLength(int length){
        if (length > 0) {
            this.length = length;
        }
        else this.length = 0;

    }

    public float getCost(){
        return this.cost;
    }

    public void setCost(float cost){
        if (cost > 0) {
            this.cost = cost;
        }
        else this.cost = 0.0f;
    }

    //Các phương thức khởi tạo
    //Đặc điểm của constructor:
    //++ tên trùng với tên lớp
    //++ không có kiểu trả về, không có void
    //++ xây dựng nhiều constructor giúp khởi tạo
    //++ đối tượng từ lớp theo nhiều cách khác nhau


    public DigitalVideoDisc(String id, String title, float cost) {
        super(id, title, cost);
    }

    public DigitalVideoDisc(String id, String title, int length, float cost) {
        super(id, title, cost);
        this.length = length;
    }

    public DigitalVideoDisc(String id, String title, String category, int length, float cost ) {
        super(id, title, category, cost);
        this.length = length;
    }

    public DigitalVideoDisc(String id, String title, String category, int length, String director, float cost) {
        this(id, title, category, length, cost);
        this.director = director;
    }

    //Các phương thức khác
    //Phương thức in thông in của đối tượng DVD
    public void printInfo(){
        System.out.println("------------DVD Info------------");
        System.out.println("Title: " + this.title);
        System.out.println("Category: " + this.category);
        System.out.println("Director: " + this.director);
        System.out.println("Length: " + this.length);
        System.out.println("Cost: " + this.cost);
    }
    public boolean search(String title) {
        int  i = 0;
        String[] titleItems = title.split("\\s");
        String[] currentTitleItems = this.getTitle().split("\\s");
        for (String item : titleItems) {
            for (String titleItem: currentTitleItems)
                if (item.equals(titleItem)) {
                    i++;
                }
        }
        if ( i == currentTitleItems.length) {
            return true;
        } else
            return false;
    }
}
