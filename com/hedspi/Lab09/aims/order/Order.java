package com.hedspi.Lab09.aims.order;

import com.hedspi.Lab09.aims.disc.DigitalVideoDisc;
import com.hedspi.Lab09.aims.media.Book;
import com.hedspi.Lab09.aims.media.CompactDisc;
import com.hedspi.Lab09.aims.media.Media;
import com.hedspi.Lab09.aims.media.Track;
import com.hedspi.Lab09.aims.utils.MyDate;

import java.util.ArrayList;
import java.util.Scanner;

public class Order {
    //Khai bao 1 hằng số: số lượng sản phẩm tối đa cho đơn hàng
    public static final int MAX_NUMBER_ORDERED = 10;
    //Khai báo 1 mảng các đối tượng DVD
//    private DigitalVideoDisc itemOrdered[] =
//            new DigitalVideoDisc[MAX_NUMBER_ORDERED];
    public ArrayList<Media> itemOrdered = new ArrayList<Media>();

    //Khai báo thuộc tính chứa số lượng sản phẩm hiện có trong đơn hàng
    private int qtyOrdered = 0;

    //Xây dựng các phương thức
    //Các phương thức getter setter

    public int getQtyOrdered(){
        return this.qtyOrdered;
    }

    private MyDate dateOrder;
    public static final int MAX_LIMITED_ORDERED = 5;
    public static int nbOrder = 0;

    public Order() {
        if (this.nbOrder == MAX_LIMITED_ORDERED) {
            System.out.println("Over number of order!");
        }
        else{
            this.qtyOrdered = 0;
            this.dateOrder = new MyDate(); //măc định là ngày hiện tại
            nbOrder++; //có thêm đối tượng đã được tạo ra
        }
    }

    public static Order getOrder() {
        if(nbOrder < MAX_LIMITED_ORDERED) {
            Order newOrder = new Order();
            nbOrder++;
            System.out.println("Creat new order successfully!");
            return newOrder;
        }
        else {
            System.out.println("The numbers of orders is almost max.");
            return null;
        }
    }
    public void setQtyOrdered(int qtyOrdered){
        if (qtyOrdered >= 0) {
            this.qtyOrdered = qtyOrdered;
        }
    }


    public void addMedia(Media media) {
        if (itemOrdered.contains(media)) {
            System.err.println("The media with title: " + media.getTitle() + " is existed!");
        } else {
            itemOrdered.add(media);
            System.out.println("The media with title: " + media.getTitle() + " has been added.");
        }
    }

    public void addMedia(Media... mediaList) {
        for(int i = 0; i < mediaList.length; i++) {
            addMedia(mediaList[i]);
        }
    }

    public boolean removeMedia(String id) {
        boolean tmp = false;
        for(Media media: itemOrdered) {
            if(media.getId().equalsIgnoreCase(id)) {
                removeMedia(media);
                tmp = true;
                break;
            }
        }
        if(tmp == false) {
            System.err.println("Id isn't founded!");
        }
        return tmp;
    }

    public void removeMedia(Media media) {
        if(itemOrdered.contains(media)) {
            System.out.println("Media with ID: " + media.getId() + " has been deleted!");
            itemOrdered.remove(media);
        }else {
            System.err.println("The media may not exist!");
        }
    }


    public float totalCost(){
        float total = 0.0f;
        for(int i = 0; i < this.qtyOrdered; i++)
        {
            total += itemOrdered.get(i).getCost();
        }
        return total;
    }

    //Phương thức in đơn hàng
    public void printListOrder(){
        System.out.println("Order");
        System.out.print("Date: ");
        this.dateOrder.printDate();
        System.out.println("Ordered Items:");
        System.out.println("\t Title \t \t Category \t Director \t Length\t   Price");
        for(int i = 0; i < this.qtyOrdered; i++){
            System.out.println((i+1) + ". DVD - " + itemOrdered.get(i).getTitle() + "\t"
                    + itemOrdered.get(i).getCategory() + "\t"
                    + " : " + itemOrdered.get(i).getCost() + "$");
        }

        System.out.println("Total cost: " + this.totalCost());
        System.out.println();
    }

    public Media getALuckyItem() {
        int LuckyNumber = (int)Math.floor(Math.random() * itemOrdered.size());
//        this.itemOrdered.get(LuckyNumber).setCost(0.0f);
        return itemOrdered.get(LuckyNumber);
    }
    public boolean checkId(String id) {
        for(Media media: itemOrdered) {
            if(media.getId().equalsIgnoreCase(id)) {
                return true;
            }
        }
        return false;
    }
    public static void addBookToOrder(Order anOrder) {
        Scanner sc = new Scanner(System.in);
        System.out.print("\tNhap id: ");
        String id = sc.nextLine();
        System.out.print("\tNhap title: ");
        String title = sc.nextLine();
        System.out.print("\tNhap category: ");
        String category = sc.nextLine();
        System.out.print("\tNhap cost: ");
        float cost = sc.nextFloat();
        // constructor a book without AuthorsList
        Book aBook = new Book(id, title, category, cost);
        System.out.print("\tNhap so luong authors: ");
        int iAuthors = sc.nextInt();
        while(iAuthors <= 0) {
            System.err.println("So luong authors phai lon hon 0");
            System.out.print("\tNhap so luong authors: ");
            iAuthors = sc.nextInt();
        }
        sc.nextLine();
        while(iAuthors > 0) {
            System.out.print("\tNhap author: ");
            String author = sc.nextLine();
            aBook.addAuthor(author);
            iAuthors--;
        }
        anOrder.addMedia(aBook);
    }
    public static void addDvdToOrder(Order anOrder) {
        Scanner sc = new Scanner(System.in);
        System.out.printf("\tNhap id: ");
        String id = sc.nextLine();
        System.out.printf("\tNhap title: ");
        String title = sc.nextLine();
        System.out.printf("\tNhap category: ");
        String category = sc.nextLine();
        System.out.printf("\tNhap director: ");
        String director = sc.nextLine();
        System.out.printf("\tNhap cost: ");
        float cost = sc.nextFloat();
        System.out.printf("\tNhap length: ");
        int length = sc.nextInt();
        sc.nextLine();
        DigitalVideoDisc dvd = new DigitalVideoDisc(id, title, category, length, director, cost);
        String ask;
        do {
            System.out.printf("***\tBan muon nghe thu khong(yes|no): ");
            ask = sc.nextLine();
            switch (ask) {
                case "yes":
                    System.out.println("===============================");
                    dvd.play();
                    System.out.println("===============================");
                    break;
                case "no":
                    break;
                default:
                    System.err.println("Nhap sai cu phap");
            }
        } while (ask.equalsIgnoreCase("yes") == false && ask.equalsIgnoreCase("no") == false);
        anOrder.addMedia(dvd);
    }

    public static void addCdToOrder(Order anOrder) {
        Scanner sc = new Scanner(System.in);
        System.out.printf("\tNhap id: ");
        String id = sc.nextLine();
        System.out.printf("\tNhap title: ");
        String title = sc.nextLine();
        System.out.printf("\tNhap category: ");
        String category = sc.nextLine();
        System.out.printf("\tNhap artist: ");
        String artist = sc.nextLine();
        System.out.printf("\tNhap cost: ");
        float cost = sc.nextFloat();
        CompactDisc cd = new CompactDisc(id, title, category, artist, cost);
        System.out.printf("\tSo luong Track: ");
        int count = sc.nextInt();
        while(count <= 0) {
            System.err.println("So luong Track phai lon hon 0");
            System.out.printf("\tSo luong Track: ");
            count = sc.nextInt();
        }
        sc.nextLine();
        Track track = null;
        String titleTrack;
        int lengthTrack;
        for(int i = 0; i < count; i++) {
            System.out.println("***\tTrack " + (i+1));
            System.out.printf("\tNhap title cua track: ");
            titleTrack = sc.nextLine();
            System.out.printf("\tNhap length cua track: ");
            lengthTrack = sc.nextInt();
            track = new Track(titleTrack, lengthTrack);
            cd.addTrack(track);
            sc.nextLine();
        }
        String ask;
        do {
            System.out.printf("***\tBan muon nghe thu khong(yes|no): ");
            ask = sc.nextLine();
            switch (ask) {
                case "yes":
                    System.out.println("===============================");
                    cd.play();
                    System.out.println("===============================");
                    break;
                case "no":
                    break;
                default:
                    System.err.println("Nhap sai cu phap");
            }
        } while (ask.equalsIgnoreCase("yes") == false && ask.equalsIgnoreCase("no") == false);
        anOrder.addMedia(cd);
    }

    public static void removeMediaInOrder(Order anOrder) {
        Scanner sc = new Scanner(System.in);
        System.out.print("Nhap id: ");
        String id = sc.nextLine();
        anOrder.removeMedia(id);
    }
}
