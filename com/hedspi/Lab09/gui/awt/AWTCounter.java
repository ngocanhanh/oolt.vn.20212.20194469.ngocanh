package com.hedspi.Lab09.gui.awt;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class AWTCounter extends Frame implements ActionListener {
    private Label lblCount;
    private TextField tfCount;
    private Button btnCount;
    private int count;

    public AWTCounter() {
        this.setLayout(new FlowLayout());
        lblCount = new Label("Counter");
        this.add(lblCount);

        tfCount = new TextField(count + "", 10);
        tfCount.setEditable(false);
        this.add(tfCount);

        btnCount = new Button("Count");
        this.add(btnCount);
        btnCount.addActionListener(this);

        this.setTitle("AWT Counter");
        this.setSize(250,100);
        this.setVisible(true);
    }
    public void actionPerformed(ActionEvent e) {
        ++count;
        this.tfCount.setText(count + "");
    }
    public static void main(String[] args) {
        AWTCounter app =new AWTCounter();
    }

}
